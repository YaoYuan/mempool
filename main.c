#include <stdio.h>
#include <stdlib.h>

#include "mempool.h"

int main(void)
{
  mempool pool;
  initPool(&pool);

  char* p1 = NULL;
  char* p2 = NULL;
  char* p3 = NULL;
  char* p4 = NULL;
  char* p5 = NULL;
  char* p6 = NULL;

  newMem(&pool, 16, &p1);
  newMem(&pool, 24, &p2);
  newMem(&pool, 125, &p3);
  newMem(&pool, 512, &p4);
  newMem(&pool, 32, &p5);
  newMem(&pool, 22, &p6);

  releaseMem(&pool, p1);
  releaseMem(&pool, p6);
  releaseMem(&pool, p2);
  char* p7 = p3;
  retain(&pool, p3);
  releaseMem(&pool, p3);

/*
  if (newmemo)
  {
    printf("addr : %x\n", p);
  }
  else
  {
    printf("new memory failed!\n");
  }
*/

  logMem(&pool);

  return 0;
}
