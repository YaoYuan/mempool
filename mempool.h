#ifndef __mempool_h__
#define __mempool_h__

#include <stdbool.h>
#include <stdio.h>

#define MEM_MAX_SIZE (1024*5)

typedef struct tag_memInfoBlock
{
  char* addr;
  unsigned int size;
  unsigned int ref;
} memInfoBlock;

typedef struct tag_mempool
{
  char mem[MEM_MAX_SIZE];
  memInfoBlock idleList[MEM_MAX_SIZE];
  memInfoBlock usedList[MEM_MAX_SIZE];
  int idleCnt;
  int usedCnt;
} mempool;

void initPool(mempool* pM)
{
  if (!pM)
  {
      return;
  }

  unsigned int i = 0;
  for (; i < MEM_MAX_SIZE; ++i)
  {
    pM->idleList[i].addr = 0;
    pM->idleList[i].size = 0;
    pM->idleList[i].ref = 0;

    pM->usedList[i].addr = 0;
    pM->usedList[i].size = 0;
    pM->usedList[i].ref = 0;
  }

  pM->idleList[0].addr = &(pM->mem[0]);
  pM->idleList[0].size = MEM_MAX_SIZE;
  pM->idleCnt = 1;
  pM->usedCnt = 0;
}

int getFixedSize(int i)
{
  int ex = i % 2;

  if (ex)
  {
    return i + 1;
  }
  else
  {
    return i;
  }
}

int getIdleCnt(mempool* pM)
{
  if (!pM)
  {
    return 0;
  }

  unsigned int i = 0;
  for (; i < MEM_MAX_SIZE; ++i)
  {
    if (pM->idleList[i].addr == 0)
    {
      return i;
    }
  }

  return 0;
}

int getUsedCnt(mempool* pM)
{
  if (!pM)
  {
    return 0;
  }

  unsigned int i = 0;
  for (; i < MEM_MAX_SIZE; ++i)
  {
    if (pM->usedList[i].addr == 0)
    {
      return i;
    }
  }

  return 0;
}

void rerangeIdle(mempool* pM)
{
  if (!pM)
  {
    return;
  }

  unsigned int cnt = 0;
  unsigned int i = MEM_MAX_SIZE;

  for (; cnt < i; ++cnt)
  {
    if (cnt != i - 1)
    {
      if (pM->idleList[cnt].addr == 0 && pM->idleList[cnt + 1].addr != 0)
      {
        pM->idleList[cnt].addr = pM->idleList[cnt + 1].addr;
        pM->idleList[cnt + 1].addr = 0;

        pM->idleList[cnt].size = pM->idleList[cnt + 1].size;
        pM->idleList[cnt + 1].size = 0;

        pM->idleList[cnt].ref = pM->idleList[cnt + 1].ref;
        pM->idleList[cnt + 1].ref = 0;
      }
    }
  }

  pM->idleCnt = getIdleCnt(pM);
}

void reorderUsed(mempool* pM)
{
  if (!pM)
  {
    return;
  }

  unsigned int cnt  = 0;
  unsigned int inner = 0;
  unsigned int endian = pM->usedCnt;     //getUsedCnt(pM);
  for (; cnt < endian - 1; ++cnt)
  {
    for (inner = 0; inner < endian - 1 - cnt; ++inner)
    {
      if (pM->usedList[inner].size > pM->usedList[inner + 1].size)
      {
        char* addr = pM->usedList[inner].addr;
        pM->usedList[inner].addr = pM->usedList[inner + 1].addr;
        pM->usedList[inner + 1].addr = addr;

        int size = pM->usedList[inner].size;
        pM->usedList[inner].size = pM->usedList[inner + 1].size;
        pM->usedList[inner + 1].size = size;

        int ref = pM->usedList[inner].ref;
        pM->usedList[inner].ref = pM->usedList[inner + 1].ref;
        pM->usedList[inner + 1].ref = ref;
      }
    }
  }
}

void reorderIdle(mempool* pM)   ///need rerange first.
{
  if (!pM)
  {
    return;
  }

  unsigned int cnt  = 0;
  unsigned int inner = 0;
  unsigned int endian = pM->idleCnt;   //getIdleCnt(pM);
  for (; cnt < endian - 1; ++cnt)
  {
    for (inner = 0; inner < endian - 1 - cnt; ++inner)
    {
      if (pM->idleList[inner].size > pM->idleList[inner + 1].size)
      {
        char* addr = pM->idleList[inner].addr;
        pM->idleList[inner].addr = pM->idleList[inner + 1].addr;
        pM->idleList[inner + 1].addr = addr;

        unsigned int size = pM->idleList[inner].size;
        pM->idleList[inner].size = pM->idleList[inner + 1].size;
        pM->idleList[inner + 1].size = size;

        unsigned int ref = pM->idleList[inner].ref;
        pM->idleList[inner].ref = pM->idleList[inner + 1].ref;
        pM->idleList[inner + 1].ref = ref;
      }
    }
  }
}

void reorder(mempool* pM)       ///desc by size, null addr all at the end.
{
  reorderUsed(pM);
  combineIdle(pM);
}

bool addToIdle(mempool* pM, int usedId)
{
  if (!pM || usedId >= pM->usedCnt)
  {
    return false;
  }

  pM->idleList[pM->idleCnt].addr = pM->usedList[usedId].addr;
  pM->idleList[pM->idleCnt].size = pM->usedList[usedId].size;
  pM->idleList[pM->idleCnt].ref = 0;

  pM->idleCnt += 1;

  return true;
}

bool delFromIdle(mempool* pM, int index)
{
  if (!pM || pM->idleCnt <= index)
  {
    return false;
  }

  pM->idleList[index].addr = pM->idleList[pM->idleCnt - 1].addr;
  pM->idleList[index].size = pM->idleList[pM->idleCnt - 1].size;
  pM->idleList[index].ref = pM->idleList[pM->idleCnt - 1].ref;

  pM->idleList[pM->idleCnt - 1].addr = 0;
  pM->idleList[pM->idleCnt - 1].size = 0;
  pM->idleList[pM->idleCnt - 1].ref = 0;
  pM->idleCnt -= 1;

  return true;
}

bool addToUsed(mempool* pM, char* addr, int size)
{
  if (!pM || pM->usedCnt >= MEM_MAX_SIZE)
  {
    return false;
  }

  pM->usedList[pM->usedCnt].addr = addr;
  pM->usedList[pM->usedCnt].size = size;
  pM->usedList[pM->usedCnt].ref = 1;

  pM->usedCnt += 1;

  return true;
}

bool delFromUsed(mempool* pM, int index)
{
  if (!pM || index >= pM->usedCnt)
  {
    return false;
  }

  addToIdle(pM, index);

  pM->usedList[index].addr = pM->usedList[pM->usedCnt - 1].addr;
  pM->usedList[index].size = pM->usedList[pM->usedCnt - 1].size;
  pM->usedList[index].ref = pM->usedList[pM->usedCnt - 1].ref;

  pM->usedList[pM->usedCnt - 1].addr = 0;
  pM->usedList[pM->usedCnt - 1].size = 0;
  pM->usedList[pM->usedCnt - 1].ref = 0;

  pM->usedCnt -= 1;

  return true;
}

void combineIdle(mempool* pM)
{
  if (!pM)
  {
    return;
  }

  rerangeIdle(pM);

  unsigned int cnt = pM->idleCnt;
  unsigned int i = 0;
  for (; i < cnt; ++i)
  {
    unsigned int j = 0;
    for (; j < cnt; ++j)
    {
      if (pM->idleList[i].addr == 0 ||
          pM->idleList[j].addr == 0 ||
          i == j)
      {
        continue;
      }

      if (pM->idleList[i].addr + pM->idleList[i].size == pM->idleList[j].addr)
      {
        pM->idleList[i].size += pM->idleList[j].size;
        pM->idleList[j].addr = 0;
        pM->idleList[j].size = 0;
      }
      else if (pM->idleList[i].addr == pM->idleList[j].addr + pM->idleList[j].size)
      {
        pM->idleList[j].size += pM->idleList[i].size;
        pM->idleList[i].addr = 0;
        pM->idleList[i].size = 0;
      }
    }
  }

  rerangeIdle(pM);
  reorderIdle(pM);
}

void retain(mempool* pM, char* addr)
{
  if (!pM)
  {
    return;
  }

  unsigned int i = 0;
  for (; i < MEM_MAX_SIZE; ++i)
  {
    if (pM->usedList[i].addr == addr)
    {
      pM->usedList[i].ref += 1;
      return;
    }
  }
}

bool newMem(mempool* pM, int size, char** paddr)
{
  if (!pM)
  {
    return false;
  }

  unsigned int i = 0;
  for (; i < pM->idleCnt; ++i)
  {
    if (pM->idleList[i].size > size)
    {
      addToUsed(pM, pM->idleList[i].addr, size);

      (*paddr) = pM->idleList[i].addr;
      pM->idleList[i].size -= size;
      pM->idleList[i].addr += size;

      reorder(pM);

      return true;
    }
    else if (pM->idleList[i].size == size)
    {
      addToUsed(pM, pM->idleList[i].addr, pM->idleList[i].size);

      (*paddr) = pM->idleList[i].addr;
      delFromIdle(pM, i);

      reorder(pM);

      return true;
    }
  }

  return false;
}

bool delMem(mempool* pM, int i)
{
  if (!pM || i >= pM->usedCnt)
  {
    return false;
  }

  delFromUsed(pM, i);
  reorder(pM);

  return false;
}

bool releaseMem(mempool* pM, char* addr)
{
  if (!pM)
  {
    return false;
  }

  unsigned int i = 0;
  for (; i < pM->usedCnt; ++i)
  {
    if (pM->usedList[i].addr == 0)
    {
      break;
    }

    if (pM->usedList[i].addr == addr)
    {
      pM->usedList[i].ref -= 1;

      if (pM->usedList[i].ref <= 0)
      {
        delMem(pM, i);
      }

      return true;
    }
  }

  return false;
}

void logMem(mempool* pM)
{
  if (!pM)
  {
    return;
  }

  reorder(pM);

  printf("***********mempool log**************\n");
  printf("idleCnt : %d\t usedCnt : %d\n", pM->idleCnt, pM->usedCnt);
  printf("\n");
  printf("idlelist : \n");

  unsigned int i = 0;
  for (; i < MEM_MAX_SIZE; ++i)
  {
    if (pM->idleList[i].addr == 0)
    {
      break;
    }
    printf("(%x, %d), ", pM->idleList[i].addr, pM->idleList[i].size);
  }

  printf("\n");
  printf("usedlist : \n");
  i = 0;
  for (; i < MEM_MAX_SIZE; ++i)
  {
    if (pM->usedList[i].addr == 0)
    {
      break;
    }
    printf("(%x, %d, %d), ", pM->usedList[i].addr, pM->usedList[i].size, pM->usedList[i].ref);
  }

  printf("\n");
  printf("************************************\n");
}

#endif // __mempool_h__
